pragma solidity ^0.8.0;

import "@openzeppelin/contracts/access/Ownable.sol";

//Ajout 1 : Limite de temps
//Ajout 2 : Révocation des votes
//Ajout 3 : Attribution de droits de vote pondérés
//Ajout 4 : Mécanisme de proposition automatique

contract Voting is Ownable {
    //Ajout 4
    uint public registeringVotersEndTime;
    uint public proposalsRegistrationStartedEndTime;
    uint public proposalsRegistrationEndedEndTime;
    uint public votingSessionStartedEndTime;

    enum WorkflowStatus {
        RegisteringVoters,
        ProposalsRegistrationStarted,
        ProposalsRegistrationEnded,
        VotingSessionStarted,
        VotingSessionEnded,
        VotesTallied
    }

    struct Voter {
        bool isRegistered;
        bool hasVoted;
        uint votedProposalId;
        uint weight;
    }

    struct Proposal {
        string description;
        uint voteCount;
    }    

    WorkflowStatus public workflowStatus;
    uint public winningProposalId;
    uint public endTime;
    uint public proposalCount;
    uint public maxProposal = 5;
    mapping(address => Voter) public voters;
    Proposal[] public proposals;

    event VoterRegistered(address indexed voterAddress, uint weight);
    event WorkflowStatusChange(WorkflowStatus previousStatus, WorkflowStatus newStatus);
    event ProposalRegistered(uint proposalId);
    event Voted(address indexed voter, uint proposalId);
    event VoteRevoked(address indexed voter, uint proposalId);

    modifier atStage(WorkflowStatus _stage) {
        require(workflowStatus == _stage, "Etape invalide");
        _;
    }

    modifier onlyRegisteredVoter() {
        require(voters[msg.sender].isRegistered, "Le votant n'est pas enregistre");
        _;
    }

    function getVoterData(address _voter) public view returns (bool, bool, uint, uint) {
        Voter memory voter = voters[_voter];
        return (voter.isRegistered, voter.hasVoted, voter.votedProposalId, voter.weight);
    }

    constructor() Ownable(msg.sender) {
        workflowStatus = WorkflowStatus.RegisteringVoters;
    }

    function startProposalsRegistration(uint _endTime) public onlyOwner atStage(WorkflowStatus.RegisteringVoters) {
        workflowStatus = WorkflowStatus.ProposalsRegistrationStarted;
        proposalsRegistrationStartedEndTime = _endTime;
        emit WorkflowStatusChange(WorkflowStatus.RegisteringVoters, WorkflowStatus.ProposalsRegistrationStarted);
    }

    function endProposalsRegistration() public onlyOwner atStage(WorkflowStatus.ProposalsRegistrationStarted) {
        workflowStatus = WorkflowStatus.ProposalsRegistrationEnded;
        proposalsRegistrationEndedEndTime = block.timestamp;
        emit WorkflowStatusChange(WorkflowStatus.ProposalsRegistrationStarted, WorkflowStatus.ProposalsRegistrationEnded);
    }

    function startVotingSession(uint _endTime) public onlyOwner atStage(WorkflowStatus.ProposalsRegistrationEnded) {
        workflowStatus = WorkflowStatus.VotingSessionStarted;
        votingSessionStartedEndTime = _endTime;
        emit WorkflowStatusChange(WorkflowStatus.ProposalsRegistrationEnded, WorkflowStatus.VotingSessionStarted);
    }

    function endVotingSession() public onlyOwner atStage(WorkflowStatus.VotingSessionStarted) {
        workflowStatus = WorkflowStatus.VotingSessionEnded;
        endTime = block.timestamp;
        emit WorkflowStatusChange(WorkflowStatus.VotingSessionStarted, WorkflowStatus.VotingSessionEnded);
    }

    function tallyVotes() public onlyOwner atStage(WorkflowStatus.VotingSessionEnded) {
        workflowStatus = WorkflowStatus.VotesTallied;
        uint winningCount = 0;
        for (uint i = 0; i < proposalCount; i++) {
            if (proposals[i].voteCount > winningCount) {
                winningCount = proposals[i].voteCount;
                winningProposalId = i;
            }
        }
        emit WorkflowStatusChange(WorkflowStatus.VotingSessionEnded, WorkflowStatus.VotesTallied);
    }

    function registerVoter(address _voter, uint _weight) public onlyOwner atStage(WorkflowStatus.RegisteringVoters) {
        require(!voters[_voter].isRegistered, "Le vote a deja ete pris en compte");
        voters[_voter] = Voter(true, false, 0, _weight);
        emit VoterRegistered(_voter, _weight);
    }

    function submitProposal(string memory _description) public onlyRegisteredVoter atStage(WorkflowStatus.ProposalsRegistrationStarted) {
        require(block.timestamp < proposalsRegistrationStartedEndTime, "Trop tard");
        proposals.push(Proposal(_description, 0));
        proposalCount++;
        emit ProposalRegistered(proposalCount - 1);
    }

    function vote(uint _proposalId) public onlyRegisteredVoter atStage(WorkflowStatus.VotingSessionStarted) {
        require(block.timestamp < votingSessionStartedEndTime, "Trop tard");
        require(!voters[msg.sender].hasVoted, "Le votant a deja vote");
        voters[msg.sender].hasVoted = true;
        voters[msg.sender].votedProposalId = _proposalId;
        proposals[_proposalId].voteCount += voters[msg.sender].weight;
        emit Voted(msg.sender, _proposalId);
    }

    //Ajout 2
    function revokeVote() public onlyRegisteredVoter atStage(WorkflowStatus.VotingSessionStarted) {
        require(block.timestamp < votingSessionStartedEndTime, "Trop tard");
        require(voters[msg.sender].hasVoted, "Le votant n'a pas encore vote");
        uint votedProposalId = voters[msg.sender].votedProposalId;
        voters[msg.sender].hasVoted = false;
        voters[msg.sender].votedProposalId = 0;
        proposals[votedProposalId].voteCount -= voters[msg.sender].weight;
        emit VoteRevoked(msg.sender, votedProposalId);
    }

    //Ajout 3
    function setVoterWeight(address _voter, uint _weight) public onlyOwner atStage(WorkflowStatus.RegisteringVoters) {
        require(voters[_voter].isRegistered, "Le vote n'est pas enregistre");
        voters[_voter].weight = _weight;
    }

    //Ajout 4
    function autoSubmitProposalIfSupported(string memory _description, uint _minVotesRequired) public onlyRegisteredVoter atStage(WorkflowStatus.ProposalsRegistrationStarted) {
        require(block.timestamp < proposalsRegistrationStartedEndTime, "Trop tard");
        require(proposalCount < maxProposal, "Le nombre maximum de proposition a deja ete atteint");
        require(voters[msg.sender].weight >= _minVotesRequired, "Rang de l'utilisateur insuffisant pour choisir automatiquement");
        proposals.push(Proposal(_description, 0));
        proposalCount++;
        emit ProposalRegistered(proposalCount - 1);
    }
}
