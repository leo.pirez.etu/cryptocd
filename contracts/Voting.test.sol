pragma solidity ^0.8.0;

import "remix_tests.sol";
import "./Voting.sol";

contract VotingTest {
    Voting voting;

    function beforeAll() public {
        voting = new Voting();
    }

    function beforeEach() public {
        
    }

    function testRegisterVoter() public {
        address voter = address(0x652c9ACcC53e765e1d96e2455E618dAaB79bA595);
        uint weight = 1;

        voting.registerVoter(voter, weight);
        (bool isRegistered, , , uint registeredWeight) = voting.getVoterData(voter);

        Assert.ok(isRegistered, "L'electeur devrait etre enregistre");
        Assert.equal(registeredWeight, weight, "Le poids de l'electeur devrait etre correct");
    }

    function testWorkflow() public {
        address voter = address(0x652c9ACcC53e765e1d96e2455E618dAaB79bA595);
        uint weight = 1;
        string memory proposalDescription = "Proposition de test";
        uint proposalId = 0;

        // Etape 1 : Enregistrement de l'electeur
        voting.registerVoter(voter, weight);

        // Etape 2 : Soumission de proposition
        voting.submitProposal(proposalDescription);
        uint proposalCount = voting.proposalCount();
        Assert.equal(proposalCount, 1, "Le nombre de propositions devrait etre de 1");

        // Etape 3 : Vote
        voting.vote(proposalId);

        (, bool hasVoted, uint votedProposalId, uint voterWeight) = voting.getVoterData(voter);
        (string memory votedProposalDescription, ) = voting.proposals(votedProposalId);

        Assert.ok(hasVoted, "L'electeur devrait avoir vote");
        Assert.equal(votedProposalId, proposalId, "L'ID de la proposition votee devrait etre correct");
        Assert.equal(votedProposalDescription, proposalDescription, "La description de la proposition votee devrait etre correcte");

        // Revocation du vote
        voting.revokeVote();
        (, bool hasRevoked, , ) = voting.getVoterData(voter);
        Assert.ok(hasRevoked, "L'electeur ne devrait pas avoir vote");

        // Calcul des votes
        voting.tallyVotes();
        uint winningProposalId = voting.winningProposalId();
        Assert.equal(winningProposalId, proposalId, "L'ID de la proposition gagnante devrait etre correct");
    }
}
